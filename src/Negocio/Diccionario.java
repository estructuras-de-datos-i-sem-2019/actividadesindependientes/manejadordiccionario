/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Palabra;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 *
 * @author madar
 */
public class Diccionario {
    
    
    private VectorGenerico<Palabra> palabras;

    public Diccionario() {
    }
    
    
    public void cargar(String urlDiccionario) {
        
        // :)
    }
    
    
    public boolean buscarPalabra(String palabra_A_buscar)
    {
     //Debe convertir la palabra_A_buscar a un objeto palabra:
        Palabra myPalabra=new Palabra(palabra_A_buscar);
        /**
         * Su proceso debe realizarse con el objeto de la clase Palabra
         * myPalabra
         */
        return false;
    }
    
    
    
    public int buscarPatron(String patron, String patron_Nuevo)
    {
        //Debe convertir el patrón a una palabra:
        Palabra myPatron=new Palabra(patron);
        Palabra myPatron_Nuevo=new Palabra(patron_Nuevo);
        /**
         * Su proceso debe realizarse con el objeto de la clase Palabra
         * myPatron y myPatron_Nuevo
         */
        return -1;
    }     
            
    /**
     *  Retorna la información del diccionario
     * @return una cadena con la información del diccionario
     */
    public String getImprimir()
    {
        return "";
    }
    
    
    
    
}
